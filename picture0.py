from flask import Flask, request
from werkzeug.utils import secure_filename
import cv2
import numpy as np
import base64

# RFB setup
from src_ultra.vision.ssd.config.fd_config import define_img_size
input_img_size = 320
define_img_size(input_img_size)
from src_ultra.vision.ssd.mb_tiny_RFB_fd import create_Mb_Tiny_RFB_fd, create_Mb_Tiny_RFB_fd_predictor
label_path = "./src_ultra/models/voc-model-labels.txt"
class_names = [name.strip() for name in open(label_path).readlines()]
num_classes = len(class_names)
test_device = "cpu"
candidate_size = 10000
threshold = 0.99
model_path = "src_ultra/models/pretrained/version-RFB-320.pth"
# model_path = "src_ultra/models/pretrained/version-RFB-640.pth"
net = create_Mb_Tiny_RFB_fd(len(class_names), is_test=True, device=test_device)
predictor = create_Mb_Tiny_RFB_fd_predictor(net, candidate_size=candidate_size, device=test_device)
net.load(model_path)
# RFB setup end

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        if 'file' not in request.files:
            return 'No file part'
        file = request.files['file']
        if file.filename == '':
            return 'No selected file'
        # rv=base64.b64encode(file.read()).decode("utf-8")
        # r1=base64.b64decode(rv.encode("utf-8"))
        buf=file.read()
        x = np.frombuffer(buf, dtype='uint8')
        gambar = cv2.imdecode(x, cv2.IMREAD_UNCHANGED)
        # r2 = np.frombuffer(r1, dtype=np.uint8)
        # gambar = cv2.imdecode(r2, flags=1)
        try:
            w, h = gambar.shape[:2]
        except:
            return 'corrupted image'
        
        inputimage = cv2.cvtColor(gambar, cv2.COLOR_BGR2RGB)
        boxes, labels, probs = predictor.predict(inputimage, candidate_size / 2, threshold)

        for i in range(boxes.size(0)):
            box = boxes[i, :]
            label = f" {probs[i]:.2f}"
            box=list(map(int,box))
            cv2.rectangle(gambar, (box[0], box[1]), (box[2], box[3]), (0, 255, 0), 4)

        cnt = cv2.imencode('.png',gambar)[1]
        b64 = str(base64.b64encode(cnt).decode("utf-8"))
        return '''
            <img src='data:image/png;base64, {}'>
            <a href="javascript:window.location.href=window.location.href">
                <input type="submit" value="restart">
            </a>
            '''.format(b64)
    return '''
            <h1>Upload new File</h1>
            <form method=post enctype=multipart/form-data>
                <label for=file>Select image:</label>
                <input type=file name=file accept=".png, .jpg, .jpeg">
                <input type=submit value=Upload>
            </form>
        '''

app.run(host='0.0.0.0', port=3333)