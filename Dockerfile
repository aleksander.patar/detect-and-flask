FROM python:3.9
RUN useradd -ms /bin/bash admin
WORKDIR /app
COPY . .
RUN apt-get update
RUN apt-get install ffmpeg libsm6 libxext6  -y
RUN pip install -r req.txt
RUN chown -R admin:admin /app
RUN chmod 755 /app
USER admin
EXPOSE 3333
CMD ["python", "./picture0.py"]