# Detect dan Flask, Port 3333!

Program ini bertujuan untuk mendeteksi wajah via web (menggunakan Flask).

## Docker

Bisa dijadikan docker image. -t tag, -d detach, -p publish. Nama proyek web_static. Nama run task1.

```bash
docker build -t web_static:latest .
docker run -d -p 3333:3333 --name task1 web_static
```

## Dockerfile

untuk previlage: useradd, chown, chmod, admin

untuk dependencies: apt update, install ffmpeg, libms6, libxext6 -y, requirements (req.txt)

## Python 3.9

```bash
pip install -r req.txt
python picture0.py
```

## Cara kerja
1. Image diupload ke web, lalu disubmit, masuk request.method=='POST'.

2. Image di-file.read, lalu di-np.frombuffer, lalu di-cv2.imdecode.

3. Face Detection dengan [ultra light](https://github.com/Linzaer/Ultra-Light-Fast-Generic-Face-Detector-1MB).

4. Lalu reload page untuk kembali ke request.method=='GET'.
